package com.arun28.rest.webservice.restfulwebservice.appMain.helloWorld;

public class HelloWorldBean {

	private String message;

	HelloWorldBean(String message)
	{
		this.message = message;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
}

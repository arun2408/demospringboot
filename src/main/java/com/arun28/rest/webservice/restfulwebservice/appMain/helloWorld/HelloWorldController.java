package com.arun28.rest.webservice.restfulwebservice.appMain.helloWorld;


import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloWorldController {

	@Autowired
	private MessageSource messageSource;
	
	@GetMapping(path = "/helloWorld")
	public String helloWorld() {

		return "Welcome to hellow World";
	}
	
	@GetMapping(path="/helloWorldBean")
	public HelloWorldBean helloWorldBean() {
		return new HelloWorldBean("Hello World");
	}
	
	@GetMapping(path = "/helloWorldBean/pathVariable/{name}")
	public HelloWorldBean helloWorldPathVariable(@PathVariable String name)
	{
		return new HelloWorldBean(name);
	}
	
	@GetMapping(path = "/helloWorldI18")
	public String helloWorldI18(@RequestHeader(name="Accept-Language", required = false) Locale locale)
	{
		return messageSource.getMessage("goodMorningMessage", null, locale);
	}
}

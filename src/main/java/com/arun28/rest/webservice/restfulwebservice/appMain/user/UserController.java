package com.arun28.rest.webservice.restfulwebservice.appMain.user;

import java.net.URI;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.arun28.rest.webservice.restfulwebservice.appMain.exceptionHandle.CustomizeExceptionHandling;

@RestController
public class UserController {

	@Autowired
	private UserDaoService userDaoService;

	@GetMapping(path = "/retrieveAllUser")
	public List<User> retrieveAllUser() {
		return userDaoService.retrieveAllUser();
	}

	@GetMapping(path = "/findByUser/{id}")
	public User findByUser(@PathVariable int id) {
		User user = userDaoService.findAnUser(id);

		if (user == null) {
			throw new UserNotFoundException("id -" + id);
		} else {
			return user;
		}

	}

	@DeleteMapping(path="/deleteAnUser/{id}")
	public User deleteAnUser(@PathVariable int id)
	{
		User user = userDaoService.deleteAnUser(id);
		
		if(user==null) {
			throw new UserNotFoundException("id -" + id);
		}
		else
		{
			return user;
		}
	}
	
	@PostMapping(path = "/createUser")
	private ResponseEntity<Object> createNewUser(@Valid @RequestBody User user) {
		User saveUser = userDaoService.addNewUser(user);

		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(saveUser.getId())
				.toUri();

		return ResponseEntity.created(location).build();
	}
}

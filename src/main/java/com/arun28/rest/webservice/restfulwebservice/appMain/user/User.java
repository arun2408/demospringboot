package com.arun28.rest.webservice.restfulwebservice.appMain.user;

import java.util.Date;

import javax.validation.constraints.Max;
import javax.validation.constraints.Past;

public class User {

	private int id;
	
	@Max(value = 2,message="At least 2 character")
	private String name;
	
	@Past
	private Date birthDdate;
	
	
	public User(int id, String name, Date birthDdate) {
		super();
		this.id = id;
		this.name = name;
		this.birthDdate = birthDdate;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Date getBirthDdate() {
		return birthDdate;
	}
	public void setBirthDdate(Date birthDdate) {
		this.birthDdate = birthDdate;
	}
}

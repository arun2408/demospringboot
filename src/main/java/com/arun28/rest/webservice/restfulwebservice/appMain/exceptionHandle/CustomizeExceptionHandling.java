package com.arun28.rest.webservice.restfulwebservice.appMain.exceptionHandle;

import java.util.Date;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.arun28.rest.webservice.restfulwebservice.appMain.user.UserNotFoundException;

@ControllerAdvice
@RestController
public class CustomizeExceptionHandling extends ResponseEntityExceptionHandler{

//	@ExceptionHandler(Exception.class)
//	public final ResponseEntity<Object> handleAllExceptions(Exception ex, WebRequest request)
//	{
//		ExceptionHandling exceptionHandling = 
//				new ExceptionHandling(new Date(), ex.getMessage(),request.getDescription(false));
//		
//		return new ResponseEntity(exceptionHandling,HttpStatus.INTERNAL_SERVER_ERROR);
//	}
	
	@ExceptionHandler(UserNotFoundException.class)
	public  ResponseEntity<Object> handleUserNotFoundException(UserNotFoundException ex, WebRequest request)
	{
		ExceptionHandling exceptionHandling = 
				new ExceptionHandling(new Date(), ex.getMessage(),request.getDescription(false));
		
		return new ResponseEntity<Object>(exceptionHandling,HttpStatus.INTERNAL_SERVER_ERROR);
	}
	
	@Override
	protected ResponseEntity<Object> handleMethodArgumentNotValid(
			MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {

		ExceptionHandling exceptionHandling = 
				new ExceptionHandling(new Date(), "Validation Failed",ex.getBindingResult().toString());
		
		return new ResponseEntity<Object>(exceptionHandling,HttpStatus.BAD_REQUEST);
	}
}

package com.arun28.rest.webservice.restfulwebservice.appMain.user;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.springframework.stereotype.Component;

@Component
public class UserDaoService {

	private static List<User> userlist = new ArrayList<User>();

	static {
		userlist.add(new User(1, "arun", new Date()));
		userlist.add(new User(2, "Ajith", new Date()));
	}

	@SuppressWarnings("unused")
	public List<User> retrieveAllUser() {

		return userlist;
	}

	@SuppressWarnings("unused")
	public User addNewUser(User user) {
		userlist.add(user);
		return user;
	}

	@SuppressWarnings("unused")
	public User findAnUser(int id) {
		for (User user2 : userlist) {
			if (user2.getId() == id) {
				return user2;
			}
		}
		return null;
	}

	public User deleteAnUser(int id) {

		Iterator<User> c = userlist.iterator();

		User user=null;
		while (c.hasNext()) {

			if (c.next().getId() == id) {
				user = c.next();
				c.remove();
			}

		}

		return user;
	}

}
